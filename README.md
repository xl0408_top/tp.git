# TP

### 介绍
一款简洁的拟态浏览器标签页

### 安装教程

1.  下载压缩包，上传至服务器即可，本项目属于静态项目

### 使用说明

1. 播放器使用`梨花带雨`，自行注册，把index.html底部的代码更换
注：播放器有域名绑定 
(项目中已经注释音乐播放器代码，如果使用取消注释即可)

### 问题
暂时应该不考虑手机版本，主要作用于电脑浏览器使用，后续应该会做浏览器扩展，更新时间不定
此版本`应该`为最终版本

### 演示

演示：[点击进入TP](https://xl0408.top/TP/index.html)
播放器在这无法正常加载,可以吧js文件放本地解决